<?php

namespace VkTest\Kernel;

use DI\Container;

class CliKernel
{
    private Container $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function run(): void {
        $orderCompletionFixer = $this->container->get('\VkTest\Batch\OrderCompletionFixer');
        $orderCompletionFixer->fixIncompleteOrders();

        $orderCreationFixer = $this->container->get('\VkTest\Batch\OrderCreationFixer');
        $orderCreationFixer->fixIncompleteOrders();
    }
}
