<?php
declare(strict_types=1);

namespace VkTest\Kernel;

use DI\Container;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use VkTest\Entitties\User;
use VkTest\Exceptions\WebException;
use VkTest\Storage\UserStorage;

class WebKernel
{
    private UserStorage $userStorage;

    private Container $container;

    public function __construct(UserStorage $userStorage, Container $container)
    {
        $this->userStorage = $userStorage;
        $this->container = $container;
    }

    public function run(): void {
        $request = Request::createFromGlobals();
        $response = $this->getResponse($request);
        $response->prepare($request);
        $response->send();
    }

    private function getResponse(Request $request): Response {
        try {
            $user = $this->getAuthenticatedUser($request);
            return $this->handleAuthenticatedRequest($request, $user);
        } catch (WebException $e) {
            error_log((string) $e);
            return $e->getResponse();
        } catch (\Throwable $e) {
            error_log((string) $e);
            return JsonResponse::create(
                ['error' => 'Something went wrong'],
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    private function handleAuthenticatedRequest(Request $request, User $user): Response {
        switch (true) {
            case ($request->getMethod() === Request::METHOD_GET && $request->getPathInfo() === "/orders"):
                $controller = $this->container->get('\VkTest\Controllers\ListOrdersController');
                return $controller->listOrders($request);
                break;

            case ($request->getMethod() === Request::METHOD_POST && $request->getPathInfo() === "/orders/create"):
                $controller = $this->container->get('\VkTest\Controllers\OrderController');
                return $controller->createOrder($user, $request);
                break;

            case ($request->getMethod() === Request::METHOD_POST && $request->getPathInfo() === "/orders/complete"):
                $controller = $this->container->get('\VkTest\Controllers\OrderController');
                return $controller->completeOrder($user, $request);
                break;

            case ($request->getMethod() === Request::METHOD_POST && $request->getPathInfo() === "/orders/confirm-completion"):
                $controller = $this->container->get('\VkTest\Controllers\OrderController');
                return $controller->confirmOrderCompletion($user, $request);
                break;

            default:
                return JsonResponse::create(
                    ['error' => 'Invalid request. Please check the API documentation'],
                    Response::HTTP_BAD_REQUEST
                );
        }
    }

    private function getAuthenticatedUser(Request $request): User {
        $userId = $request->getUser();
        if ($userId === null) {
            throw new WebException(
                "Unauthorized: Request doesn't contain 'Authorization' HTTP header. Please use HTTP Basic Auth.",
                Response::HTTP_UNAUTHORIZED
            );
        }
        $userId = intval($userId);
        if ($userId === 0) {
            throw new WebException(
                "Unauthorized: Request 'Authorization' HTTP header doesn't contain integer user id.",
                Response::HTTP_UNAUTHORIZED
            );
        }


        $authToken = $request->getPassword();
        if ($authToken === null) {
            throw new WebException(
                "Unauthorized: Request 'Authorization' HTTP header doesn't contain auth token. Please use HTTP Basic Auth.",
                Response::HTTP_UNAUTHORIZED
            );
        }

        $user = $this->userStorage->getUser($userId);
        if (!$user->isAuthTokenValid($authToken)) {
            throw new WebException(
                "Unauthorized: Auth token is invalid. Provided user id = $userId",
                Response::HTTP_UNAUTHORIZED
            );
        }

        return $user;
    }
}