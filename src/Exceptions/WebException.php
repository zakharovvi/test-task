<?php
declare(strict_types=1);

namespace VkTest\Exceptions;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class WebException extends \RuntimeException {

    private string $publicMessage;

    public function __construct(
        string $publicMessage = "Something went wrong",
        int $code = Response::HTTP_INTERNAL_SERVER_ERROR,
        ?string $message = null,
        Throwable $previous = null
    ) {
        if ($message === null) {
            $message = $publicMessage;
        }
        $this->publicMessage = $publicMessage;
        parent::__construct($message, $code, $previous);
    }

    public function getResponse(): Response {
        return JsonResponse::create(['error' => $this->publicMessage], $this->getCode());
    }
}
