<?php
declare(strict_types=1);

namespace VkTest\Services;

use Symfony\Component\HttpFoundation\Response;
use VkTest\Entitties\Order;
use VkTest\Exceptions\WebException;
use VkTest\Storage\OrderStorage;
use VkTest\Storage\UserStorage;

class OrderCreationService
{
    private OrderStorage $orderStorage;

    private UserStorage $userStorage;

    public function __construct(OrderStorage $orderStorage, UserStorage $userStorage)
    {
        $this->orderStorage = $orderStorage;
        $this->userStorage = $userStorage;
    }

    public function createOrder(int $ownerId, int $price, string $name): Order
    {
        $order = $this->orderStorage->createOrder($ownerId, $price, $name);
        $this->moveNewOrderToTheAvailableState($order);
        return $order;
    }

    public function moveNewOrderToTheAvailableState(Order $order): void {
        switch ($order->status()) {
            case Order::STATUS_HOLD_REQUIRED:
                $this->userStorage->lockUserHold($order);
                $this->orderStorage->moveOrderToTheNextStatus($order);
                // continue processing next state

            case Order::STATUS_HOLD_LOCKED:
                $this->userStorage->increaseUserHold($order);
                $this->orderStorage->moveOrderToTheNextStatus($order);
                break;

            default:
                throw new WebException(
                    "Order creation can't be completed at the time. Please check order status later",
                    Response::HTTP_CONFLICT,
                    "order status is invalid for creation"
                );
        }
    }
}