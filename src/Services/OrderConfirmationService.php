<?php
declare(strict_types=1);

namespace VkTest\Services;

use Symfony\Component\HttpFoundation\Response;
use VkTest\Entitties\Order;
use VkTest\Exceptions\WebException;
use VkTest\Storage\OrderStorage;
use VkTest\Storage\UserStorage;

class OrderConfirmationService
{
    private OrderStorage $orderStorage;

    private UserStorage $userStorage;

    public function __construct(
        OrderStorage $orderStorage,
        UserStorage $userBalanceService
    ) {
        $this->orderStorage = $orderStorage;
        $this->userStorage = $userBalanceService;
    }

    public function confirmOrderCompletion(int $orderId, int $ownerId): void {
        $order = $this->orderStorage->getOrder($orderId);

        if ($order->status() === Order::STATUS_COMPLETION_CLOSED) {
            throw new WebException(
                "Order completion already confirmed.",
                Response::HTTP_CONFLICT
            );
        }

        if ($order->status() > Order::STATUS_COMPLETED) {
            throw new WebException(
                "Order completion can't be confirmed at the moment. Please check order status later",
                Response::HTTP_CONFLICT
            );
        }

        if ($order->status() < Order::STATUS_COMPLETED) {
            throw new WebException(
                "Order hasn't been completed yet.",
                Response::HTTP_CONFLICT
            );
        }

        if ($order->ownerId() !== $ownerId) {
            throw new WebException(
                "Order completion can't be confirmed because order belongs to another user",
                Response::HTTP_FORBIDDEN
            );
        }

        $this->orderStorage->moveOrderToTheNextStatus($order);
        $this->moveConfirmedOrderToTheFinalState($order);
    }

    public function moveConfirmedOrderToTheFinalState(Order $order) {
        switch ($order->status()) {
            case Order::STATUS_COMPLETION_CONFIRMED:
                $this->userStorage->lockUserBalanceForCharge($order);
                $this->orderStorage->moveOrderToTheNextStatus($order);
                // continue processing next state

            case Order::STATUS_COMPLETION_OWNER_BALANCE_LOCKED:
                $this->userStorage->chargeUserBalance($order);
                $this->userStorage->lockUserBalanceForFund($order);
                $this->orderStorage->moveOrderToTheNextStatus($order);
                // continue processing next state

            case Order::STATUS_COMPLETION_EXECUTOR_BALANCE_LOCKED:
                $this->userStorage->fundUserBalance($order);
                $this->orderStorage->moveOrderToTheNextStatus($order);
                break; // order final status reached

            default:
                throw new WebException(
                    "Order confirmation can't be completed at the time. Please check order status later",
                    Response::HTTP_CONFLICT,
                    "order status is invalid for confirmation"
                );
        }
    }
}