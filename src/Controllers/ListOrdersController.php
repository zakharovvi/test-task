<?php
declare(strict_types=1);

namespace VkTest\Controllers;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use VkTest\Entitties\Order;
use VkTest\Storage\OrderPaginationStorage;

class ListOrdersController
{
    const PAGE_SIZE = 100;

    private OrderPaginationStorage $orderPaginationStorage;

    public function __construct(OrderPaginationStorage $orderPaginationStorage) {
        $this->orderPaginationStorage = $orderPaginationStorage;
    }

    public function listOrders(Request $request): Response {
        $orders = $this->fetchOrders($request);
        return $this->buildResponse($orders['orders'], $orders['check_previous_page']);
    }

    private function fetchOrders(Request $request): array {
        // получаем из бд на один заказ больше (PAGE_SIZE + 1),
        // чтобы проверить существует ли следующая страница

        switch (true) {
            case (is_numeric($request->get('before_order_id'))):
                $orders = $this->orderPaginationStorage->previousOrders(
                    intval($request->get('before_order_id')),
                    $this::PAGE_SIZE + 1
                );
                return ['orders' => $orders, 'check_previous_page' => true];
                break;

            case (is_numeric($request->get('after_order_id'))):
                $orders = $this->orderPaginationStorage->nextOrders(
                    intval($request->get('after_order_id')),
                    $this::PAGE_SIZE + 1
                );
                return ['orders' => $orders, 'check_previous_page' => true];
                break;

            case ($request->get('before_order_id') === 'newest'):
                // если запрашиваютеся последние заказы, то следующей страницы не будет,
                // поэтому не нужно запрашивать PAGE_SIZE+1 для проверки
                $orders = $this->orderPaginationStorage->newestOrders($this::PAGE_SIZE);
                return ['orders' => $orders, 'check_previous_page' => true];
                break;

            default:
                $orders = $this->orderPaginationStorage->oldestOrders($this::PAGE_SIZE + 1);
                // если запрашиваютеся первые заказы, то предыдущей страницы не будет
                return ['orders' => $orders, 'check_previous_page' => false];
                break;
        }
    }

    private function buildResponse(array $orders, bool $checkPreviousPage): Response {
        $nextPageExists = count($orders) > $this::PAGE_SIZE;
        $previousPageExists = $checkPreviousPage &&
            count($orders) > 0 &&
            count($this->orderPaginationStorage->previousOrders($orders[0]['order_id'], 1)) === 1;

        // show prices in rubles
        $func = function($order) {
            $order['price'] = number_format($order['price'] / 100, 2);
            return $order;
        };
        $ordersInRubles = array_map($func, $orders);

        $response = [
            'links' => [
                'oldest' => 'orders?after_order_id=oldest',
                'previous' => $previousPageExists ? "orders?before_order_id={$orders[0]['order_id']}" : null,
                'next' => $nextPageExists ? "orders?after_order_id={$orders[array_key_last($orders) - 1]['order_id']}" : null,
                'newest' => 'orders?before_order_id=newest',
            ],
            'orders' => array_slice($ordersInRubles, 0, $this::PAGE_SIZE),
        ];
        return JsonResponse::create($response);
    }
}