<?php
declare(strict_types=1);

namespace VkTest\Controllers;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use VkTest\Entitties\User;
use VkTest\Exceptions\WebException;
use VkTest\Services\OrderConfirmationService;
use VkTest\Services\OrderCreationService;
use VkTest\Storage\OrderStorage;
use VkTest\Storage\UserStorage;

class OrderController
{
    private OrderStorage $orderStorage;

    private UserStorage $userStorage;

    private OrderConfirmationService $orderConfirmationService;

    private OrderCreationService $orderCreationService;

    public function __construct(
        OrderStorage $orderStorage,
        UserStorage $userBalanceService,
        OrderConfirmationService $orderConfirmationService,
        OrderCreationService $orderCreationService
    ) {
        $this->orderStorage = $orderStorage;
        $this->userStorage = $userBalanceService;
        $this->orderConfirmationService = $orderConfirmationService;
        $this->orderCreationService = $orderCreationService;
    }

    public function createOrder(User $user, Request $request): Response
    {
        $orderData = json_decode($request->getContent(), true);
        if ($orderData === null) {
            return JsonResponse::create(
                ['error' => "Can't decode order data"],
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!array_key_exists('name', $orderData) || !is_string($orderData['name'])) {
            return JsonResponse::create(
                ['error' => "Can't decode order name"],
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
        if (strlen($orderData['name']) > 255) {
            return JsonResponse::create(
                ['error' => "Order name too long. Should be less then 255 chars"],
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
        if (strlen($orderData['name']) === 0) {
            return JsonResponse::create(
                ['error' => "Order name is empty"],
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        $price = $this->getPrice($orderData);

        // проверка не гарантирует, что заказ получится успешно создать
        // в случае большого количества конкурентных запросов на создание заказов для этого пользователя
        if (!$user->isBalanceSufficientForANewOrder($price)) {
            return JsonResponse::create(
                ['error' => "Order can't be created. Please increase your balance"],
                Response::HTTP_PAYMENT_REQUIRED
            );
        }

        $order = $this->orderCreationService->createOrder($user->id(), $price, $orderData['name']);
        return JsonResponse::create(['order_id' => $order->id()], Response::HTTP_CREATED);
    }

    public function completeOrder(User $user, Request $request): Response
    {
        $orderId = $this->getOrderId($request);
        $this->orderStorage->completeOrder($orderId, $user->id());
        return Response::create(null, Response::HTTP_NO_CONTENT);
    }

    public function confirmOrderCompletion(User $user, Request $request): Response
    {
        $orderId = $this->getOrderId($request);
        $this->orderConfirmationService->confirmOrderCompletion($orderId, $user->id());
        return Response::create(null, Response::HTTP_NO_CONTENT);
    }

    private function getOrderId(Request $request): int {
        $orderId = $request->get('order_id');
        if (!is_numeric($orderId)) {
            throw new WebException(
                "order_id GET parameter should be positive integer. Provided $orderId",
                Response::HTTP_BAD_REQUEST
            );
        }

        $orderId = intval($orderId);
        if ($orderId === 0) {
            throw new WebException(
                "order_id GET parameter should be positive integer. Provided $orderId",
                Response::HTTP_BAD_REQUEST
            );
        }

        return $orderId;
    }

    private function getPrice(array $orderData): int {
        if (!array_key_exists('price', $orderData) || !is_numeric($orderData['price'])) {
            throw new WebException(
                "Can't decode order data",
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        // конвертируем рубли в копейки
        $price = intval($orderData['price'] * 100);

        // intval возвращает 0, если передан невалидный integer или он превышает PHP_INT_MAX
        if ($price <= 0) {
            throw new WebException(
                "Order price must be positive integer",
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        return $price;
    }
}