<?php
declare(strict_types=1);

namespace VkTest\Entitties;

use Symfony\Component\HttpFoundation\Response;
use VkTest\Exceptions\WebException;

class Order
{
    // Начальный статус заказа при создании.
    // Требуется холд суммы заказа у заказчика.
    public const STATUS_HOLD_REQUIRED = 0;

    // Установлена блокировка на заказчике для увеличения холда на сумму этого заказа.
    // Заказчик не может создавать новые заказы, пока эта блокировка не будет снята.
    public const STATUS_HOLD_LOCKED = 1;

    // Только заказы в этом статусе отображаются в списке доступных для выполнения
    // Цена заказа добавлена в сумму холда у заказчика
    public const STATUS_AVAILABLE = 2;

    // Заказ выполнен пользователем.
    // Заказ больше не отображается в списке доступных заказов.
    public const STATUS_COMPLETED = 3;

    // Выполнение заказа подтверждено заказчиком
    public const STATUS_COMPLETION_CONFIRMED = 4;

    // Установлена блокировка на заказчике для списания баланса и холда на сумму этого заказа.
    public const STATUS_COMPLETION_OWNER_BALANCE_LOCKED = 5;

    // Установлена блокировка на исполнителе для увеличения баланса на сумму этого заказа минус комиссия системы
    public const STATUS_COMPLETION_EXECUTOR_BALANCE_LOCKED = 6;

    // Все операции с балансами заказчика и исполнителя успешно выполнены
    public const STATUS_COMPLETION_CLOSED = 7;



    // комиссия системы в процентах
    private const FEE_PERCENT = 0.12345;

    private int $orderId;

    private int $ownerId;

    private int $price;

    private ?int $executorId;

    private int $status;

    public function __construct(array $orderData) {
        $this->orderId = $orderData['order_id'];
        $this->ownerId = $orderData['owner_id'];
        $this->price = $orderData['price'];
        $this->executorId = $orderData['executor_id'];
        $this->status = $orderData['status'];
    }

    public function id(): int
    {
        return $this->orderId;
    }

    public function ownerId(): int
    {
        return $this->ownerId;
    }

    public function price(): int
    {
        return $this->price;
    }

    public function priceMinusFee(): int {
        if (self::FEE_PERCENT >= 100) {
            throw new WebException(
                "Something went wrong",
                Response::HTTP_INTERNAL_SERVER_ERROR,
                "Platform fee is greater or equal then 100% for order $this->orderId"
            );
        }

        if (self::FEE_PERCENT <= 0) {
            throw new WebException(
                "Something went wrong",
                Response::HTTP_INTERNAL_SERVER_ERROR,
                "Platform fee is less or equal then 0% for order $this->orderId"
            );
        }

        // округляем размер комиссию вверх в пользу системы
        $fee = ceil($this->price * self::FEE_PERCENT / 100);
        if ($fee === false) {
            throw new WebException(
                "Something went wrong",
                Response::HTTP_INTERNAL_SERVER_ERROR,
                "fee round failed for order $this->orderId"
            );
        }

        $fee = intval($fee);
        if ($fee === 0) {
            throw new WebException(
                "Something went wrong",
                Response::HTTP_INTERNAL_SERVER_ERROR,
                "fee is zero for order $this->orderId"
            );
        }

        return $this->price - $fee;
    }

    public function executorId(): int
    {
        return $this->executorId;
    }

    public function status(): int
    {
        return $this->status;
    }

    public function incrementStatus() {
        $this->status++;
    }
}