<?php
declare(strict_types=1);

namespace VkTest\Entitties;

class User
{
    private int $userId;

    private string $authTokenHash;

    private int $amountBalance;

    private int $amountHold;

    private ?int $localHoldOrderId;

    public function __construct(array $userData)
    {
        $this->userId = $userData['user_id'];
        $this->authTokenHash = $userData['auth_token_hash'];
        $this->amountBalance = $userData['amount_balance'];
        $this->amountHold = $userData['amount_hold'];
        $this->localHoldOrderId = $userData['lock_hold_order_id'];
    }

    public function id(): int
    {
        return $this->userId;
    }

    public function localHoldOrderId(): ?int {
        return $this->localHoldOrderId;
    }

    public function isBalanceSufficientForANewOrder(int $price): bool {
        return $this->amountHold + $price <= $this->amountBalance;
    }

    public function isAuthTokenValid(string $authToken): bool {
        return password_verify($authToken, $this->authTokenHash);
    }
}