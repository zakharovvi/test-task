<?php
declare(strict_types=1);

namespace VkTest\Storage;

use ParagonIE\EasyDB\EasyDB;
use Symfony\Component\HttpFoundation\Response;
use VkTest\Entitties\Order;
use VkTest\Exceptions\WebException;

class OrderStorage
{
    private EasyDB $db;

    public function __construct(EasyDB $db)
    {
        $this->db = $db;
    }

    public function getOrder(int $orderId): Order {
        $orderData = $this->db->row(<<<SQL
SELECT
    order_id,
    owner_id,
    executor_id,
    price,
    status
FROM orders 
WHERE order_id = ?;
SQL
            ,
            $orderId
        );

        if ($orderData === [] || $orderData === null) {
            throw new WebException(
                "Order not found",
                Response::HTTP_NOT_FOUND
            );
        }

        return new Order($orderData);
    }

    public function getIncompleteCreatedOrders(int $batchSize): array {
        return $this->getIncompleteOrders(
            $batchSize,
            Order::STATUS_HOLD_REQUIRED,
            Order::STATUS_AVAILABLE
        );
    }

    public function getIncompleteConfirmedOrders(int $batchSize): array {
        return $this->getIncompleteOrders(
            $batchSize,
            Order::STATUS_COMPLETION_CONFIRMED,
            Order::STATUS_COMPLETION_CLOSED
        );
    }

    private function getIncompleteOrders(int $batchSize, int $statusGreaterThenOrEqual, int $statusLessThen): array {
        $ordersData = $this->db->safeQuery(<<<SQL
SELECT
    order_id,
    owner_id,
    executor_id,
    price,
    status
FROM orders 
WHERE 
    status >= ?
    AND status < ?
LIMIT ?;
SQL
            ,
            [$statusGreaterThenOrEqual, $statusLessThen, $batchSize]
        );

        $func = function($orderData) {
            return new Order($orderData);
        };
        return array_map($func, $ordersData);
    }

    public function createOrder(int $ownerId, int $price, string $name): Order
    {
        $this->db->safeQuery(
            <<<SQL
INSERT INTO orders 
    (owner_id, price, name) 
    VALUES(?, ?, ?);
SQL
            ,
            [$ownerId, $price, $name]
        );
        $orderId = intval($this->db->lastInsertId());
        if ($orderId === 0) {
            throw new WebException(
                "Something went wrong",
                Response::HTTP_INTERNAL_SERVER_ERROR,
                "PDO lastInsertId is zero just after order a new order insertion"
            );
        }

        return new Order([
            'order_id' => $orderId,
            'owner_id' => $ownerId,
            'executor_id' => null,
            'price' => $price,
            'status' => Order::STATUS_HOLD_REQUIRED,
        ]);
    }

    public function completeOrder(int $orderId, int $executorId): void {
        $affectedRows = $this->db->safeQuery(
            <<<'SQL'
UPDATE orders 
SET 
    executor_id = ?, 
    status = ?
WHERE 
    order_id = ? AND 
    executor_id IS NULL AND
    status = ?;
SQL
            ,
            [
                $executorId,
                Order::STATUS_COMPLETED,
                $orderId,
                Order::STATUS_AVAILABLE
            ],
            EasyDB::DEFAULT_FETCH_STYLE,
            true
        );

        if ($affectedRows === 0) {
            $order = $this->getOrder($orderId); // check that order exists

            if ($order->status() >= Order::STATUS_COMPLETED) {
                throw new WebException(
                    "Order already completed",
                    Response::HTTP_CONFLICT
                );
            }

            throw new WebException(
                "Order can't be completed",
                Response::HTTP_CONFLICT
            );
        }
    }

    public function moveOrderToTheNextStatus(Order $order): void {
        if ($order->status() === Order::STATUS_AVAILABLE) {
            throw new \InvalidArgumentException('Order in STATUS_AVAILABLE should be completed only by OrderStorage::completeOrder');
        }

        $affectedRows = $this->db->safeQuery(
            <<<'SQL'
UPDATE orders 
SET status = status + 1
WHERE 
      order_id = ? AND 
      owner_id = ? AND
      status = ?;
SQL
            ,
            [
                $order->id(),
                $order->ownerId(),
                $order->status()
            ],
            EasyDB::DEFAULT_FETCH_STYLE,
            true
        );

        if ($affectedRows === 0) {
            throw new WebException(
                "Order change request can't be processed at the time.",
                Response::HTTP_CONFLICT
            );
        }

        $order->incrementStatus();
    }
}