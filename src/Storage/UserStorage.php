<?php
declare(strict_types=1);

namespace VkTest\Storage;

use ParagonIE\EasyDB\EasyDB;
use Symfony\Component\HttpFoundation\Response;
use VkTest\Entitties\Order;
use VkTest\Entitties\User;
use VkTest\Exceptions\WebException;

class UserStorage
{
    private EasyDB $db;

    public function __construct(EasyDB $db)
    {
        $this->db = $db;
    }

    public function getUser(int $userId): User {
        $userData = $this->db->row(<<<SQL
SELECT
   user_id,
   auth_token_hash,
   amount_balance,
   amount_hold,
   lock_hold_order_id
FROM users 
WHERE user_id = ?;
SQL
            ,
            $userId,
        );

        if ($userData === [] || $userData === null) {
            throw new WebException(
                "Unauthorized: User not found by id. Provided user id = $userId",
                Response::HTTP_UNAUTHORIZED
            );
        }

        return new User($userData);
    }

    public function lockUserHold(Order $order): void {
        // колонка lock_hold_attempt нужна, чтобы $affectedRows был больше нуля во всех случаях, когда order соответсвует предикату
        // "OR lock_hold_attempt = ?" нужно для ситуаций, когда мы восстанавливаем консистентность данных и нужно вернуть $affectedRows=1,
        // даже если лок уже был поставлен для этого $orderId
        $affectedRows = $this->db->safeQuery(
            <<<'SQL'
UPDATE users 
SET 
    lock_hold_order_id = ?, 
    lock_hold_attempt = lock_hold_attempt + 1
WHERE 
      user_id = ?
      AND (lock_hold_order_id IS NULL OR lock_hold_order_id = ?);
SQL
            ,
            [$order->id(), $order->ownerId(), $order->id()],
            EasyDB::DEFAULT_FETCH_STYLE,
            true
        );

        if ($affectedRows === 0) {
            throw new WebException(
                "Order confirmation can't be completed at the time. Please check order status later",
                Response::HTTP_CONFLICT,
                'user hold already locked or user doesnt exist'
            );
        }
    }

    public function lockUserBalanceForCharge(Order $order): void {
        // колонка lock_charge_attempt нужна, чтобы $affectedRows был больше нуля во всех случаях, когда order соответсвует предикату
        // "OR lock_charge_order_id = ?" нужно для ситуаций, когда мы восстанавливаем консистентность данных и нужно вернуть $affectedRows=1,
        // даже если лок уже был поставлен для этого $orderId
        $affectedRows = $this->db->safeQuery(
            <<<'SQL'
UPDATE users 
SET 
    lock_charge_order_id = ?, 
    lock_charge_attempt = lock_charge_attempt + 1
WHERE 
      user_id = ?
      AND (lock_charge_order_id IS NULL OR lock_charge_order_id = ?);
SQL
            ,
            [$order->id(), $order->ownerId(), $order->id()],
            EasyDB::DEFAULT_FETCH_STYLE,
            true
        );

        if ($affectedRows === 0) {
            throw new WebException(
                "Order confirmation can't be completed at the time. Please check order status later",
                Response::HTTP_CONFLICT,
                'user balance already locked or user doesnt exist'
            );
        }
    }

    public function lockUserBalanceForFund(Order $order): void {
        // колонка lock_fund_attempt нужна, чтобы $affectedRows был больше нуля во всех случаях, когда order соответсвует предикату
        // "OR lock_fund_order_id = ?" нужно для ситуаций, когда мы восстанавливаем консистентность данных и нужно вернуть $affectedRows=1,
        // даже если лок уже был поставлен для этого $orderId
        $affectedRows = $this->db->safeQuery(
            <<<'SQL'
UPDATE users 
SET 
    lock_fund_order_id = ?, 
    lock_fund_attempt = lock_fund_attempt + 1
WHERE 
      user_id = ?
      AND (lock_fund_order_id IS NULL OR lock_fund_order_id = ?);
SQL
            ,
            [$order->id(), $order->executorId(), $order->id()],
            EasyDB::DEFAULT_FETCH_STYLE,
            true
        );

        if ($affectedRows === 0) {
            throw new WebException(
                "Order confirmation can't be completed at the time. Please check order status later",
                Response::HTTP_CONFLICT,
                'user balance already locked or user doesnt exist'
            );
        }
    }

    public function chargeUserBalance(Order $order): void {
        $this->db->safeQuery(
            <<<'SQL'
UPDATE users 
SET 
    amount_balance = amount_balance - ?,
    amount_hold = amount_hold - ?,
    lock_charge_order_id = null,
    lock_charge_attempt = 0
WHERE 
      user_id = ?
      AND lock_charge_order_id = ?;
SQL
            ,
            [$order->price(), $order->price(), $order->ownerId(), $order->id()],
        );

        // не проверяем affected rows:
        // 1. при первом выполнении не проверять результат безопасно,
        // т.к. единственный способ снять lock_charge_order_id это изменить баланс
        // 2. в ситуациях когда мы восстанавливаем консистентность данных
        // при повторном выполнении lock_charge_order_id будет уже снят и баланс пользователя измениться не должен
    }

    public function fundUserBalance(Order $order): void {
        $this->db->safeQuery(
            <<<'SQL'
UPDATE users
SET 
    amount_balance = amount_balance + ?,
    lock_fund_order_id = null,
    lock_fund_attempt = 0
WHERE 
      user_id = ?
      AND lock_fund_order_id = ?;
SQL
            ,
            [$order->priceMinusFee(), $order->executorId(), $order->id()],
        );

        // не проверяем affected rows:
        // 1. при первом выполнении не проверять результат безопасно,
        // т.к. единственный способ снять lock_fund_order_id это изменить баланс
        // 2. в ситуациях когда мы восстанавливаем консистентность данных
        // при повторном выполнении lock_fund_order_id будет уже снят и баланс пользователя измениться не должен
    }

    public function increaseUserHold(Order $order): void {
        $affectedRows = $this->db->safeQuery(
            <<<'SQL'
UPDATE users 
SET 
    amount_hold = amount_hold + ?,
    lock_hold_order_id = null,
    lock_hold_attempt = 0
WHERE 
      user_id = ?
      AND lock_hold_order_id = ?
      AND amount_hold + ? <= amount_balance;
SQL
            ,
            [$order->price(), $order->ownerId(), $order->id(), $order->price()],
            EasyDB::DEFAULT_FETCH_STYLE,
            true
        );

        if ($affectedRows === 0) {
            $refreshedUser = $this->getUser($order->ownerId());

            $localHoldOrderId = $refreshedUser->localHoldOrderId();
            if ($localHoldOrderId === null) {
                // если lock_hold_order_id = null, значит
                // amount_hold уже был увеличен в предыдущий раз
                // в этом случае можно перевести order на следующий статус
                return;
            }

            if ($localHoldOrderId !== $order->id()) {
                // баланс заблокирован другим order_id. Запрос будет повторяться пока lock_hold_order_id не освободится
                throw new WebException(
                    "Order creation can't be completed at the time. Please check order status later",
                    Response::HTTP_CONFLICT,
                    "user {$refreshedUser->id()} hold locked by order $localHoldOrderId."
                );
            }


            throw new WebException(
                "Order creation can't be completed. Please increase your balance",
                Response::HTTP_PAYMENT_REQUIRED,
            );
        }
    }
}
