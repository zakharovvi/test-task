<?php

namespace VkTest\Storage;

use ParagonIE\EasyDB\EasyDB;
use VkTest\Entitties\Order;

class OrderPaginationStorage
{
    private EasyDB $db;

    public function __construct(EasyDB $db)
    {
        $this->db = $db;
    }

    public function previousOrders(int $nextId, int $limit): array {
        return $this->db->safeQuery(<<<SQL
SELECT 
    order_id,
    name,
    price
FROM orders 
WHERE 
    status = ?
    AND order_id < ?
ORDER BY order_id
LIMIT ?;
SQL
            ,
            [Order::STATUS_AVAILABLE, $nextId, $limit]
        );
    }

    public function nextOrders(int $previousId, int $limit): array {
        return $this->db->safeQuery(<<<SQL
SELECT 
    order_id,
    name,
    price
FROM orders 
WHERE 
    status = ?
    AND order_id > ?
ORDER BY order_id
LIMIT ?;
SQL
            ,
            [Order::STATUS_AVAILABLE, $previousId, $limit]
        );
    }

    public function oldestOrders(int $limit): array {
        return $this->db->safeQuery(<<<SQL
SELECT 
    order_id,
    name,
    price
FROM orders 
WHERE status = ?
ORDER BY order_id
LIMIT ?;
SQL
            ,
            [Order::STATUS_AVAILABLE, $limit]
        );
    }

    public function newestOrders(int $limit): array {
        return $this->db->safeQuery(<<<SQL
SELECT * FROM (
    SELECT 
        order_id,
        name,
        price
    FROM orders 
    WHERE status = ?
    ORDER BY order_id DESC
    LIMIT ?
) a
ORDER BY order_id;
SQL
            ,
            [Order::STATUS_AVAILABLE, $limit]
        );
    }
}