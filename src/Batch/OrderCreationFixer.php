<?php


namespace VkTest\Batch;

use VkTest\Services\OrderCreationService;
use VkTest\Storage\OrderStorage;

class OrderCreationFixer
{
    private const INCOMPLETE_ORDERS_BATCH_SIZE = 1000;

    private OrderStorage $orderStorage;

    private OrderCreationService $orderCreationService;

    public function __construct(
        OrderStorage $orderStorage,
        OrderCreationService $orderCreationService
    ) {
        $this->orderStorage = $orderStorage;
        $this->orderCreationService = $orderCreationService;
    }

    public function fixIncompleteOrders(): void {
        $orders = $this->orderStorage->getIncompleteCreatedOrders(self::INCOMPLETE_ORDERS_BATCH_SIZE);
        foreach ($orders as $order) {
            try {
                $this->orderCreationService->moveNewOrderToTheAvailableState($order);
            } catch (\Throwable $e) {
                error_log((string) $e);
            }
        }
    }
}