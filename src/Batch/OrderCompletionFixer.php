<?php


namespace VkTest\Batch;

use VkTest\Services\OrderConfirmationService;
use VkTest\Storage\OrderStorage;

class OrderCompletionFixer
{
    private const INCOMPLETE_ORDERS_BATCH_SIZE = 1000;

    private OrderStorage $orderStorage;

    private OrderConfirmationService $orderStateService;

    public function __construct(
        OrderStorage $orderStorage,
        OrderConfirmationService $orderStateService
    ) {
        $this->orderStorage = $orderStorage;
        $this->orderStateService = $orderStateService;
    }

    public function fixIncompleteOrders(): void {
        $orders = $this->orderStorage->getIncompleteConfirmedOrders(self::INCOMPLETE_ORDERS_BATCH_SIZE);
        foreach ($orders as $order) {
            try {
                $this->orderStateService->moveConfirmedOrderToTheFinalState($order);
            } catch (\Throwable $e) {
                error_log((string) $e);
            }
        }
    }
}