<?php
declare(strict_types=1);

use ParagonIE\EasyDB\EasyDB;
use ParagonIE\EasyDB\Factory;

$builder = new DI\ContainerBuilder();
$builder->addDefinitions([
    EasyDB::class => Factory::fromArray(['mysql:dbname=vk_test;host=mysql', 'root', 'password']),
]);
return $builder->build();