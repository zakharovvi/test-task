<?php
declare(strict_types=1);

require __DIR__ . '/../vendor/autoload.php';

$container = require_once __DIR__ . '/bootstrap.php';

$cliKernel = new \VkTest\Kernel\CliKernel($container);
$cliKernel->run();
