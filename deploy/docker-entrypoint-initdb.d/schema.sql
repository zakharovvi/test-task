CREATE SCHEMA IF NOT EXISTS `vk_test` DEFAULT CHARACTER SET utf8mb4;

CREATE TABLE `vk_test`.`users` (
    `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'User ID.',
    `auth_token_hash` varchar(255) NOT NULL COMMENT 'User password hashed by bcrypt.',
    `amount_balance` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'User balance in rubles cents.',
    `amount_hold` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'User hold in ruble cents which is used for check that the user cannot create orders worth more than his balance.',
    `lock_charge_order_id` bigint(20) unsigned DEFAULT NULL COMMENT 'Order ID (orders.order_id), which is used to block amount_balance decrease.',
    `lock_fund_order_id` bigint(20) unsigned DEFAULT NULL COMMENT 'Order ID (orders.order_id), which is used to block amount_balance increase.',
    `lock_charge_attempt` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of attempts to block amount_balance decrease which is used to restore data integrity in the background process.',
    `lock_fund_attempt` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of attempts to block amount_balance increase which is used to restore data integrity in the background process.',
    `lock_hold_order_id` bigint(20) unsigned DEFAULT NULL COMMENT 'Order ID (orders.order_id), to block amount_hold increase.',
    `lock_hold_attempt` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of attempts to block amount_hold increase which is used to restore data integrity in the background process.',
    PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_bin;

CREATE TABLE `vk_test`.`orders` (
    `order_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Order ID.',
    `name` varchar(255) NOT NULL COMMENT 'Order name.',
    `price` int(11) unsigned NOT NULL COMMENT 'Order price in ruble cents.',
    `owner_id` int(11) unsigned NOT NULL COMMENT 'ID of order owner (users.user_id).',
    `executor_id` int(11) unsigned DEFAULT NULL COMMENT 'ID of order executor (users.user_id).',
    `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Order status code.',
    PRIMARY KEY (`order_id`),
    KEY `idx_status_order_id` (`status`,`order_id`) COMMENT 'Index is used for available orders listing in GET /orders.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_bin;
