INSERT INTO `vk_test`.`users`
    (`auth_token_hash`,`amount_balance`,`lock_charge_order_id`,`lock_fund_order_id`,`lock_charge_attempt`,`lock_fund_attempt`,`amount_hold`,`lock_hold_order_id`,`lock_hold_attempt`)
    VALUES
           ('$2y$10$3icrLYAdY8qeiP2OMOutmO6ARcrXV.nxdNJCrXCUVk6S8.bHzB9..',1000000000,NULL,NULL,0,0,0,NULL,0),
           ('$2y$10$flVmdhNP/TUStUiG7Xrew..T9bKPR32n.9nZ4N5PP96ptpV4fjk.C',1000000000,NULL,NULL,0,0,0,NULL,0),
           ('$2y$10$J6AnlCGBWIiScVlU49mO5u.tQtQyU0745DzP7gXR8ezV2jkGNPxHu',0,NULL,NULL,0,0,0,NULL,0)
;
