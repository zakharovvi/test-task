<?php
declare(strict_types=1);

require __DIR__ . '/../vendor/autoload.php';

$container = require_once __DIR__ . '/../src/bootstrap.php';

$webKernel = new \VkTest\Kernel\WebKernel(
    $container->get('\VkTest\Storage\UserStorage'),
    $container
);
$webKernel->run();
